# OMniLeads: Analisis de sentimiento

#### This project is part of OMniLeads - 100% Open-Source Contact Center Software

#### [Community discord](https://discord.gg/FEDkVmSQ)

This component implements an integration with the APIs of OpenAI and GCP. The component is capable of receiving requests for sentiment analysis on survey recordings or call center calls, then analyzing the audio transcription with the specified language model within each AI provider (OpenAI or GCP), and returning the detected sentiment.

Each provider allows for fine-tuning of models using operation data to improve upon the base model. You must indicate as an environment variable which AI provider to use. You will need to have an account with either of them to access the sentiment analysis functionality.

## Environment variable

The following list of environment variables needs to be passed when running the component as part of the OMniLeads suite.

```
TZ=
PGHOST=
PGDATABASE=
PGPORT=
PGUSER=
PGPASSWORD=
REDIS_HOSTNAME=
REDIS_PORT=
RABBITMQ_HOST=
S3_BUCKET_NAME=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
S3_ENDPOINT=
S3_ENDPOINT=
OPENAI_API_KEY=
GOOGLE_API_KEY=
GOOGLE_CLOUD_PROJECTID=
GOOGLE_APPLICATION_CREDENTIALS=/tmp/google_credential.json
```

The requests are received through a RabbitMQ queue and are sent from the OMniLeads views that list the recordings or from the post-call survey module of the ACD, which launches a query about the experience in the service received by the agent.

![Diagrama deploy tool](png/sentiment_analysis.png)




