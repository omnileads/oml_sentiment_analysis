#!/usr/bin/env python3.10

import pika
import os
import json
import boto3
from datetime import date
import speech_recognition as sr
import tempfile
import logging
import psycopg2
import google.generativeai as gemini

# Configura el logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Configuración de credenciales y bucket de S3
aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')
bucket_name = os.getenv('S3_BUCKET_NAME')
bucket_url = os.getenv('S3_ENDPOINT')
# Configuración de RabbitMQ
rabbitmq_server = os.getenv('RABBITMQ_HOST', 'rabbitmq')
rabbitmq_queue = 'sentiment_analysis'
# Configuración de GEMINI
gemini.api_key = os.getenv('GOOGLE_API_KEY')
project_id = os.getenv('GCP_PROJECT_ID')
model = gemini.GenerativeModel('gemini-pro')

location = "us-central1"

# Conexión a RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_server))
channel = connection.channel()
channel.queue_declare(queue=rabbitmq_queue, durable=True)

# Crear un cliente de S3
s3 = boto3.client('s3', endpoint_url=bucket_url, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

# Crear un cliente de postgres
conn = psycopg2.connect(
                host=os.getenv('PGHOST'),
                port=os.getenv('PGPORT'),
                dbname=os.getenv('PGDATABASE'),
                user=os.getenv('PGUSER'),
                password=os.getenv('PGPASSWORD')
            )
cur = conn.cursor()

# Postgres insert sentiment analysis result 
def insert_result(time, callid, calificacion, url_audio, url_transcripcion):    
    try:        
        query = """
        INSERT INTO reportes_app_analisissentimiento (time, callid, result, url_audio, url_transcription)
        VALUES (%s, %s, %s, %s, %s)
        """
        cur.execute(query, (time, callid, calificacion, url_audio, url_transcripcion))
        conn.commit()
        logging.info("Resultado insertado en la base de datos.")
    except Exception as e:
        logging.error(f"Error al insertar el resultado en la base de datos: {e}")
        conn.rollback()

# Función de callback para procesar mensajes de la cola
def callback(ch, method, properties, body):
    logging.info("Mensaje recibido desde RabbitMQ.")
    message = json.loads(body)
    file_name = message.get('fileName', '')
    uniqueId = message.get('uniqueId', '')
    callDate = message.get('callDate', '')
    lang = message.get('language', '')

    if not file_name:
        logging.error("El mensaje no contiene 'fileName'.")
        return

    logging.info(f"Archivo a procesar: {file_name}")

    # Descargar el archivo de S3
    with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
        local_file_name = tmp_file.name
    try:
        logging.info(f"Descargando {file_name} de S3 a {local_file_name}")
        s3.download_file(bucket_name, file_name, local_file_name)
    except Exception as e:
        logging.error(f"Error al descargar {file_name} de S3: {e}")
        return

    # Callrec transcription with GCP
    try:
        logging.info(f"Transcribiendo audio de {local_file_name}")
        r = sr.Recognizer()
        with sr.AudioFile(local_file_name) as source:
            audio = r.record(source)
        recognized_text = r.recognize_google_cloud(audio, language=lang)
        logging.info(f'Transcripción: "{recognized_text}"')
    except Exception as e:
        logging.error(f"Error en la transcripción de {file_name}: {e}")
        return
    finally:
        os.remove(local_file_name)

    # Enviar a GEMINI para análisis
    try:
        prompt = f"Analiza la devolución de esta encuesta de satisfaccion: {recognized_text}\n\n¿Esta devolución es: positiva, neutral o negativa?"
        response = model.generate_content(prompt)
        result = response.text
        logging.info(f"El resultado es: {result}")
    except Exception as e:
        logging.error(f"Error al enviar prompt a GEMINI: {e}")

    # Llamar a funcion para insertar en postgres
    insert_result(callDate, uniqueId, result, file_name, "relleno")


# Consumir mensajes de la cola
channel.basic_consume(queue=rabbitmq_queue, on_message_callback=callback, auto_ack=True)

logging.info('Esperando mensajes. Para salir presiona CTRL+C')
channel.start_consuming()

cur.close()
conn.close()
