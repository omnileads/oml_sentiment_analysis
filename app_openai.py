#!/usr/bin/env python3.10

import pika
import os
import json
import boto3
from datetime import date
import whisper
import speech_recognition as sr
import tempfile
import logging
import psycopg2
import openai

# Configura el logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Configuración de credenciales y bucket de S3
aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID')
aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY')
bucket_name = os.getenv('S3_BUCKET_NAME')
bucket_url = os.getenv('S3_ENDPOINT')
# Configuración de RabbitMQ
rabbitmq_server = os.getenv('RABBITMQ_HOST', 'rabbitmq')
rabbitmq_queue = 'sentiment_analysis'
# Configuración de OPENAI
openai.api_key = os.getenv('OPENAI_API_KEY')
model = whisper.load_model("base")


lang = os.getenv('LANG')
location = "us-central1"

# Conexión a RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_server))
channel = connection.channel()
channel.queue_declare(queue=rabbitmq_queue, durable=True)

# Crear un cliente de S3
s3 = boto3.client('s3', endpoint_url=bucket_url, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)

# Crear un cliente de postgres
conn = psycopg2.connect(
                host=os.getenv('PGHOST'),
                port=os.getenv('PGPORT'),
                dbname=os.getenv('PGDATABASE'),
                user=os.getenv('PGUSER'),
                password=os.getenv('PGPASSWORD')
            )
cur = conn.cursor()

# inserto el resultado de postgres
def insert_result(time, callid, calificacion, url_audio, url_transcripcion):    
    try:        
        query = """
        INSERT INTO reportes_app_analisissentimiento (time, callid, result, url_audio, url_transcription)
        VALUES (%s, %s, %s, %s, %s)
        """
        cur.execute(query, (time, callid, calificacion, url_audio, url_transcripcion))
        conn.commit()
        logging.info("Resultado insertado en la base de datos.")
    except Exception as e:
        logging.error(f"Error al insertar el resultado en la base de datos: {e}")
        conn.rollback()

# Función de callback para procesar mensajes de la cola
def callback(ch, method, properties, body):
    logging.info("Mensaje recibido desde RabbitMQ.")
    message = json.loads(body)
    file_name = message.get('fileName', '')
    uniqueId = message.get('uniqueId', '')
    callDate = message.get('callDate', '')
    lang = message.get('language', '')

    if not file_name:
        logging.error("El mensaje no contiene 'fileName'.")
        return

    logging.info(f"Archivo a procesar: {file_name}")

    # Descargar el archivo de S3
    with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
        local_file_name = tmp_file.name
    try:
        print(f"Descargando {file_name} de S3 a {local_file_name}")
        s3.download_file(bucket_name, file_name, local_file_name)
    except Exception as e:
        print(f"Error al descargar {file_name} de S3: {e}")
        return

    # Callrec transcription with OpenAI Wishper
    try:
        logging.info(f"Transcribiendo audio de {local_file_name} con Whisper")
        result = model.transcribe(local_file_name, language=lang)
        recognized_text = result['text']
        logging.info(f'Transcripción: "{recognized_text}"')
    except Exception as e:
        logging.error(f"Error en la transcripción de {file_name} con Whisper: {e}")
    finally:
        os.remove(local_file_name)

    # Create prompt and send to ChatGPT
    try:
        prompt = f"Analiza la devolución de esta encuesta de satisfaccion: {recognized_text}\n\n¿Esta devolución es: positiva, neutral o negativa?"
    
        response = openai.completions.create(
            model="gpt-3.5-turbo-instruct", 
            prompt=prompt,
            max_tokens=300,
            temperature=1
            )

        logging.info(f"El resultado es: {response.choices[0].text.strip()}")        
    except Exception as e:
        print(f"Error al enviar prompt a OpenAI: {e}")

    # Imprime el resultado y su longitud
    result = response.choices[0].text.strip()
    # llamar a funcion para insertar en postgres
    insert_result(callDate, uniqueId, result, file_name, "relleno")
    

# Consumir mensajes de la cola
channel.basic_consume(queue=rabbitmq_queue, on_message_callback=callback, auto_ack=True)

print('Esperando mensajes. Para salir presiona CTRL+C')
channel.start_consuming()

cur.close()
conn.close()