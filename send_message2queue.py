#!/usr/bin/env python3.12
import sys
import os
import pika
import json

def send_to_rabbitmq(message, rabbitmq_server, rabbitmq_queue):
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=rabbitmq_server))
        channel = connection.channel()
        channel.queue_declare(queue=rabbitmq_queue, durable=True)
        channel.basic_publish(exchange='',
                              routing_key=rabbitmq_queue,
                              body=json.dumps(message),
                              properties=pika.BasicProperties(
                                  delivery_mode=2,  # Hacer el mensaje persistente
                              ))
        print(f"Mensaje enviado a la cola {rabbitmq_queue}")
        return True
    except Exception as e:
        print(f"Error al enviar mensaje a RabbitMQ: {str(e)}")
        return False
    finally:
        if 'connection' in locals():
            connection.close()

# Configuración de RabbitMQ
rabbitmq_server = os.getenv('RABBITMQ_HOST', 'localhost')  # 'rabbitmq' por defecto si no está definida la variable de entorno
rabbitmq_queue = 'sentiment_analysis'

# Suponiendo que la fecha se pasa como primer argumento al script
if len(sys.argv) < 1:
    print("Error: No se proporcionó una fecha.")
    sys.exit(1)

callrec_filename = sys.argv[1]
date = sys.argv[2]

# Datos que quieres enviar
message = {
    'fileName': callrec_filename,
    'uniqueId': 12345,
    'callDate': date,
    'language': 'es'
}

# Intentar enviar el mensaje una sola vez, con control de errores
if send_to_rabbitmq(message, rabbitmq_server, rabbitmq_queue):
    print("Mensaje enviado a RabbitMQ con éxito!")
else:
    print("Error: No se pudo enviar el mensaje a RabbitMQ.")
